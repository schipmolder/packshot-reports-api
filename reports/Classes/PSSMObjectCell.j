/*
 * PSSMObjectCell.j
 * SAM
 *
 * Created by Arjen Schipmolder on February 27, 2012.
 * Copyright 2012, Packshot Ltd All rights reserved.
 */


@implementation PSSMObjectCell: PSView {
	// PSMagicImageView imageView @accessors;
	// PSAssetApprovalZoomImageView zoomImageView @accessors;
	CPTextField labelView @accessors;
	// CPImageView approvalStatusView;
	// CPImage assetImage @accessors;
	// PSItem item @accessors;
	// // BOOL selected @accessors;
	// // BOOL highlighted @accessors;

	// CPCheckBox selectedCheckbox @accessors;
	// // CPCheckBox highlightCheckbox @accessors;

	// String approvalMode @accessors; // PSItemApprovalMode

	int columnIndex @accessors;
}

// + (CPString)defaultThemeClass {
// 	return "AssetApprovalTableColumnImageCell";
// }


// + (CPDictionary)themeAttributes {
// 	return [CPDictionary dictionaryWithJSObject:{
// 		@"background-color": [CPColor redColor],
// 		@"font-color": [CPColor blackColor],
// 		@"font": [CPFont fontWithName:"Arial" size:12]
// 	}];
// }

// - (void)setThemeState:(CPThemeState)aState {
// }

- (id)initWithFrame:aFrame {
	self = [super initWithFrame:aFrame];
	if (self) {
        [self setTheme:[CPTheme themeNamed:SAM_THEME]];

		columnIndex = -1;

		var myBounds = [self bounds];
		// approvalMode = 1;

		// imageView = [[PSMagicImageView alloc] initWithFrame:CGRectMake(0,0,100,100)];
		// [imageView setCenter:CGPointMake(myBounds.size.width/2, myBounds.size.height/2-8)];
		// [imageView setImageScaling:CPScaleProportionally];
		// [imageView setBackgroundColor:[CPColor clearColor]];
		// [imageView setAutoresizingMask:CPViewMinXMargin | CPViewMaxXMargin | CPViewMinYMargin | CPViewMaxYMargin];
		// [self addSubview:imageView];

		labelView = [[CPTextField alloc] initWithFrame:CGRectMake(2,-20,aFrame.size.width-4,30)];
		[labelView setAutoresizingMask:CPViewWidthSizable | CPViewMinYMargin];
		[labelView setAlignment:CPCenterTextAlignment];
		[labelView setLineBreakMode:CPLineBreakByTruncatingTail];
		[labelView setBackgroundColor:[CPColor clearColor]];
		[labelView setEditable:NO];
		[labelView setEnabled:NO];
		// [labelView setHidden:YES];
		[self addSubview:labelView];

		// selectedCheckbox = [[CPCheckBox alloc] initWithFrame:CGRectMake(2,-21,20,20)];
		// [selectedCheckbox setAutoresizingMask:CPViewMinYMargin | CPViewMinYMargin];
		// [selectedCheckbox setHidden:YES];
		// [selectedCheckbox setTarget:self];
		// // [selectedCheckbox setAction:@selector(selectedCheckboxChanged:)];
		// [self addSubview:selectedCheckbox];

		// // highlightCheckbox = [[CPCheckBox alloc] initWithFrame:CGRectMake(20,-20,20,20)];
		// // [highlightCheckbox setAutoresizingMask:CPViewMinYMargin | CPViewMinYMargin];
		// // [highlightCheckbox setHidden:YES];
		// // [highlightCheckbox setTarget:self];
		// // [self addSubview:highlightCheckbox];

		// approvalStatusView = [[CPImageView alloc] initWithFrame:CGRectMake(aFrame.size.width-22,0,22,22)];
		// // [approvalStatusView setCenter:CGPointMake(myBounds.size.width/2, myBounds.size.height/2-8)];
		// [approvalStatusView setImageScaling:CPScaleNone];
		// [approvalStatusView setBackgroundColor:[CPColor clearColor]];
		// [approvalStatusView setAutoresizingMask:CPViewMinXMargin | CPViewMaxYMargin];
		// [self addSubview:approvalStatusView];

		// zoomImageView = [[PSAssetApprovalZoomImageView alloc] initWithFrame:CGRectMake(aFrame.size.width-20,aFrame.size.height-20,20,20)];
		// // [zoomImageView setCenter:CGPointMake(myBounds.size.width/2, myBounds.size.height/2-8)];
		// // [zoomImageView setImageScaling:CPScaleProportionally];
		// [zoomImageView setHidden:YES];
		// [zoomImageView setBackgroundColor:[CPColor clearColor]];
		// [zoomImageView setAutoresizingMask:CPViewMinXMargin | CPViewMinYMargin];
		// [self addSubview:zoomImageView];
	}
	return self;
}

- (id)initWithCoder:(CPCoder)aCoder {
	self = [super initWithCoder:aCoder];
	if (self) {
        [self setTheme:[CPTheme themeNamed:SAM_THEME]];

		columnIndex = [aCoder decodeObjectForKey:"columnIndex"];
		// imageView = [aCoder decodeObjectForKey:"imageView"];
		// selectedCheckbox = [aCoder decodeObjectForKey:"selectedCheckbox"];
		// zoomImageView = [aCoder decodeObjectForKey:"zoomImageView"];
		labelView = [aCoder decodeObjectForKey:"labelView"];
		// assetImage = [aCoder decodeObjectForKey:"assetImage"];
		// item = [aCoder decodeObjectForKey:"item"];
		// approvalStatusView = [aCoder decodeObjectForKey:"approvalStatusView"];
		// approvalMode = [aCoder decodeObjectForKey:"approvalMode"];

		// [zoomImageView setMainImageView:imageView];
		// [[CPNotificationCenter defaultCenter] addObserver:self selector:@selector(itemHighlighted:) name:PSApprovalItemHighlightedNotification object:nil];
	}
	return self;
}

- (void)encodeWithCoder:(CPCoder)aCoder {
	[aCoder encodeObject:labelView forKey:"labelView"];
	[aCoder encodeObject:columnIndex forKey:"columnIndex"];
	// [aCoder encodeObject:imageView forKey:"imageView"];
	// [aCoder encodeObject:selectedCheckbox forKey:"selectedCheckbox"];
	// [aCoder encodeObject:zoomImageView forKey:"zoomImageView"];
	// [aCoder encodeObject:assetImage forKey:"assetImage"];
	// [aCoder encodeObject:item forKey:"item"];
	// [aCoder encodeObject:approvalStatusView forKey:"approvalStatusView"];
	// [aCoder encodeObject:approvalMode forKey:"approvalMode"];
	[super encodeWithCoder:aCoder];
}

// - (void)itemHighlighted:(CPNotification)aNotification {
// 	if (item) {
// 		var userInfo = [aNotification userInfo];
// 		var theItem = [userInfo objectForKey:@"item"];
// 		if (![[theItem nodeRef] isEqualToString:[item nodeRef]] && highlighted)
// 			[self setHighlighted:NO];
// 	}
// }

// - (void)drawRect:(CPRect)aRect {

// 	if (item == nil) {
// 		[self setBackgroundColor:[CPColor clearColor]];
// 		[labelView setHidden:YES];
// 		[selectedCheckbox setHidden:YES];
// 	} else {
// 		[self setBackgroundColor:[self currentValueForThemeAttribute:@"background-color"]];
// 		[labelView setFont:[self currentValueForThemeAttribute:@"font"]];
// 		[labelView setTextColor:[self currentValueForThemeAttribute:@"font-color"]];
// 		[labelView setHidden:NO];
// 		var isSelectable = [item isSelectableInApprovalMode:approvalMode];
// 			[selectedCheckbox setHidden:!isSelectable];
// 	}
// }

// - (void) mouseEntered:(CPEvent)anEvent {
// 	[self setThemeState:CPThemeStateHovered];
// 	[self setNeedsDisplay:YES];
// }

// - (void) mouseExited:(CPEvent)anEvent {
// 	[self unsetThemeState:CPThemeStateHovered];
// 	[self setNeedsDisplay:YES];
// }



- (void)setHighlighted:(BOOL)isHighlighted {
// debugger;
// 	var userInfo = [CPDictionary dictionaryWithObject:item forKey:@"item"];

 	if (isHighlighted) {
		[self setThemeState:CPThemeStateSelected];
// 		[[CPNotificationCenter defaultCenter] postNotificationName:PSApprovalItemHighlightedNotification object:self userInfo:userInfo];
	} else {
		[self unsetThemeState:CPThemeStateSelected];
// 		// [[CPNotificationCenter defaultCenter] postNotificationName:PSApprovalItemUnHighlightedNotification object:self userInfo:userInfo];
	}
}

// - (void)mouseDown:(CPEvent)anEvent {
// 	[item setHighlighted:YES];
// }

- (void)setColumnIndex:(int)idx {
	columnIndex = idx;
}


- (void)setObjectValue:(id)aValue {
	var theValue = @"";
	switch(columnIndex) {
        case 0:
        	theValue = aValue.sku;
			break;
        case 1:
        	theValue = aValue.sentForPhotography;
			break;
        case 2:
        	theValue = aValue.photograph;
			break;
        case 3:
        	theValue = aValue.enrichment;
			break;
        default:
        	theValue = @"";
			break;
    }
	[labelView setStringValue:theValue];
}

- (void)xxsetObjectValue:(id)aValue {

	if (item != null)
		[item removeObserver:self forKeyPath:@"highlighted"];

	var children = [aValue children];
	if ([children count] > columnIndex) {
		item = [children objectAtIndex:columnIndex];
	} else {
		item = nil;
	}
	if (item != nil) {
		if (item.selected)
			[selectedCheckbox setState:CPOnState];
		else
			[selectedCheckbox setState:CPOffState];
	} else {
		[selectedCheckbox setState:CPOffState];
	}
    [selectedCheckbox bind:CPValueBinding toObject:item withKeyPath:@"selected" options:nil];
	[item addObserver:self forKeyPath:@"highlighted" options:CPKeyValueObservingOptionNew | CPKeyValueObservingOptionOld context:nil];
	[item addObserver:self forKeyPath:@"selected" options:CPKeyValueObservingOptionNew | CPKeyValueObservingOptionOld context:nil];

	if(item != nil && [aValue class] == PSItem) {
		var imageUrl = [item imageUrl];

		[imageView setImage:[[CPImage alloc] initWithContentsOfFile:[[CPBundle mainBundle] pathForResource:@"Images/imageLoading.jpg"] size:CPSizeMake(100, 100)]];
		[imageView setHidden:NO];
		[zoomImageView setHidden:NO];
		if (![imageUrl isEqualToString:@""]) {
			assetImage = [[CPImage alloc] initWithContentsOfFile:kAPIServerUrl+imageUrl+"width=100&height=100"];
			[assetImage setDelegate:self];
		}

		var appDelegate = [[CPApplication sharedApplication] delegate];
		if (![appDelegate userIsCustomer]) {

			var approvalStatus = [item status];

			if ([approvalMode isEqualToString:PSItemApprovalModeProductOverview]) {
				var approvalIcon = "";
				if (approvalStatus == PSApprovalStatusAwaitingApprovalPhase1)
					approvalIcon = "thumbsup-icon";
				if (approvalStatus == PSApprovalStatusAwaitingApprovalPhase2)
					approvalIcon = "thumbsup-icon";
				if (approvalStatus == PSApprovalStatusAwaitingStudio)
					approvalIcon = "camera-icon";
				if (approvalStatus == PSApprovalStatusAwaitingPostProduction)
					approvalIcon = "retouch-icon";
				if (approvalIcon != "" && ![approvalIcon isEqualToString:@""])
					[approvalStatusView setImage:[[CPImage alloc] initWithContentsOfFile:"Resources/Images/approvalIcons/"+approvalIcon+".png" size:CGSizeMake(22.0, 22.0)]];
				else
					[approvalStatusView setImage:nil];

			} else {
				[approvalStatusView setImage:nil];
			}
		}

		var isSelectable = [item isSelectableInApprovalMode:approvalMode];
			[selectedCheckbox setHidden:!isSelectable];

	} else {
		item = nil;
		[imageView setHidden:YES];
		[imageView setImage:nil];
		[zoomImageView setHidden:YES];
		[approvalStatusView setImage:nil];
		[selectedCheckbox setHidden:YES];
	}
	[self display];
	// [self setNeedsDisplay:YES];
}


-(void)imageDidLoad:(CPImage)image {
	var loadedImageName = [image filename];
	var itemImageName = [item imageUrl];
	if ([loadedImageName isEqualToString:kAPIServerUrl+itemImageName+"width=100&height=100"])
		[imageView setImage:image];
}

-(void)imageDidError:(CPImage)image {
}

-(void)imageDidAbort:(CPImage)image {
}

@end

@implementation PSAssetApprovalZoomImageView: PSImageView {
	CPImageView mainImageView @accessors;
}

- (id)initWithFrame:aFrame {
	self = [super initWithFrame:aFrame];
	if (self) {
        [self setBackgroundColor:[CPColor greenColor]];
        var magImage   = [[CPImage alloc] initWithContentsOfFile:@"Resources/Images/magnifying_glass.png" size:CGSizeMake(25, 25)];
		[self setImage:magImage];

	}
	return self;
}

- (void)mouseEntered:(CPEvent)anEvent {
	if (self.mainImageView != nil) {
		[[CPNotificationCenter defaultCenter] postNotificationName:@"PSMagicImageNeedsDisplaying" object:self.mainImageView userInfo:nil];
		[[CPNotificationCenter defaultCenter] addObserver:self selector:@selector(mouseMovedInApprovalView:) name:@"PSApprovalMouseMoved" object:nil];
	}
}

- (void)mouseMovedInApprovalView:(CPNotification)aNotification {
	var userInfo = [aNotification userInfo];
	var anEvent = [userInfo objectForKey:@"event"];
	if (self.mainImageView != nil) {
		var location = [anEvent globalLocation];
		var frameRelativeToWindow = [self convertRect:[self bounds] toView:nil];

		var frameMinX = frameRelativeToWindow.origin.x;
		var frameMaxX = frameRelativeToWindow.origin.x+frameRelativeToWindow.size.width;
		var frameMinY = frameRelativeToWindow.origin.y;
		var frameMaxY = frameRelativeToWindow.origin.y+frameRelativeToWindow.size.height;

		if ((frameMinX > location.x || frameMaxX < location.x) && (frameMinY > location.y || frameMaxY < location.y)) {
			[[CPNotificationCenter defaultCenter] postNotificationName:@"PSLargeImagePreviewPanelNeedsClosing" object:self.mainImageView userInfo:nil];
			[[CPNotificationCenter defaultCenter] removeObserver:self name:@"PSApprovalMouseMoved" object:nil];
		}
	}
}


@end
