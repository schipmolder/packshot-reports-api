/*
 * PSSMReportObject.j
 * SAM
 *
 * Created by Arjen Schipmolder on February 27, 2012.
 * Copyright 2012, Packshot Ltd All rights reserved.
 */

@implementation PSSMReportObject : CPObject {
	CPString sku @accessors;
	CPString picked @accessors;
	CPString sentForPhotography @accessors;
	CPString photograph @accessors;
	CPString enrichment @accessors;
	CPString deliveryDate @accessors;

}


@end
