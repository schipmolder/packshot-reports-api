@import "/Projects/Packshot/SAM-web/App/PSImports.j"
@import "PSReportLoginPanel.j"

@implementation PSReportLoginViewController : PSLoginViewController {
	// PSPanel panel;

	// CPString username;
	// CPString password;
	// BOOL rememberCredentials;

	// PSAPI loginApi;
	// PSAPI sitesApi;
}

// - (id)init {
// 	self = [super init];
// 	if (self) {
// 	}
// 	return self;
// }

- (void)loadView {
	[super loadView];
	panel = [[PSReportLoginPanel alloc] initWithFrame:CGRectMake(-150,-50, 300, 115)];
	[panel setViewController:self];
	[panel setAutoresizingMask:CPViewMinXMargin | CPViewMaxXMargin | CPViewMinYMargin | CPViewMaxYMargin];
	[_view addSubview:panel];

	// var authCredentials = [self getAuthenticationDetailsFromCookie];
	// if (authCredentials && [authCredentials containsKey:PSAuthenticationCredentialsUsernameKey] && [authCredentials containsKey:PSAuthenticationCredentialsPasswordKey])
	// 	[panel setCredentials:authCredentials];
}

- (void)loginWithUsername:(CPString)aUsername password:(CPString)aPassword rememberMe:(BOOL)remember {
 	if (!aUsername || !aPassword || aUsername == "" || aPassword == "") {
		var shake = [[EKShakeAnimation alloc] initWithView:panel];
		username = "";
		password = "";
	} else if ((![aUsername isEqualToString:@"bijenkorf"] || ![aPassword isEqualToString:@"Bijenkorf"]) && (![aUsername isEqualToString:@"aschipmolder"] || ![aPassword isEqualToString:@"discovery"])) {
		var shake = [[EKShakeAnimation alloc] initWithView:panel];
		username = "";
		password = "";
	} else {
		var loggedInUser = [[PSUser alloc] init];
		[loggedInUser setUsername:username];
		[loggedInUser setApiTicket:@"lorem"];

		[[[CPApplication sharedApplication] delegate] setLoggedInUser:loggedInUser];
		[[CPNotificationCenter defaultCenter] postNotificationName:PSUserLoggedInNotification object:self userInfo:nil];

		// currentMode = PSLoginViewControllerSitesMode;
		currentMode = PSLoginViewControllerSiteLoadingMode;
		[panel gotoSiteLoadingMode];

		// var userInfo = [CPDictionary dictionaryWithObject:site forKey:@"site"];
		// var setSelectedSiteTimer = [CPTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(setSelectedSiteAfterDelay:) userInfo:userInfo repeats:NO];
		var setSelectedSiteTimer = [CPTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(setSelectedSiteAfterDelay:) userInfo:nil repeats:NO];
		sitesRootNode = nil;
	}
}
// - (void)loginW22ithUsername:(CPString)aUsername password:(CPString)aPassword rememberMe:(BOOL)remember {
// 	if (!aUsername || !aPassword || aUsername == "" || aPassword == "") {
// 		var shake = [[EKShakeAnimation alloc] initWithView:panel];
// 		return;
// 	}
// 	username = aUsername;
// 	password = aPassword;
// 	rememberCredentials = remember;
// 	loginApi = [PSAPI loginWithUsername:username password:password delegate:self];
// }

// - (void)getSites {
// 	sitesApi = [PSAPI getSitesWithDelegate:self];
// }

// - (void)api:(PSAPI)api didFinishLoading:(CPObject)response {

// 	if (api == loginApi) {
// 		var resultObject = response.result;
// 		var resultCode = resultObject[0].resultCode;
// 		CPLog.trace(@"Login: resultCode:" + resultCode);

// 		if (resultCode == 0) {
// 			var shake = [[EKShakeAnimation alloc] initWithView:panel];
// 			[panel showLoginFailedError];
// 			username = "";
// 			password = "";
// 			[self removeAuthenticationCookie];
// 		} else {
// 			var ticket = resultObject[0].ticket;
// 			// var isAdmin = NO;
// 			// var isAdminString = @"user";
// 			// if (resultObject[0].isAdmin == "true" || resultObject[0].isAdmin == "1") {
// 			// 	isAdmin = YES;
// 			// 	isAdminString = @"admin";
// 			// }
// 			// CPLog.trace(@"Login: Ticket:" + ticket+" ("+isAdminString+")");
// 			CPLog.trace(@"Login: Ticket:" + ticket);

// 			if (rememberCredentials) {
// 				[self addAuthenticationCookie:username password:password];
// 			}

// 			var loggedInUser = [[PSUser alloc] init];
// 			[loggedInUser setUsername:username];
// 			[loggedInUser setApiTicket:ticket];
// 			// [loggedInUser setIsAdmin:isAdmin];

// 			[[[CPApplication sharedApplication] delegate] setLoggedInUser:loggedInUser];
// 			[[CPNotificationCenter defaultCenter] postNotificationName:PSUserLoggedInNotification object:self userInfo:nil];

// 			currentMode = PSLoginViewControllerSitesMode;
// 			[self getSites];
// 		}
// 		[[[[CPApplication sharedApplication] delegate] videoView] setFrame:CGRectMake(0,0,800,600)];


// 	} else if (api == sitesApi) {

// 		var sitesObject = response.sites;
// 		var sites = [CPArray array];
// 		for (var i=0; i<[sitesObject count]; i++) {
// 			var siteProperties = [CPDictionary dictionaryWithJSObject:[[CPDictionary dictionaryWithJSObject:sitesObject[i]] objectForKey:"site"]];
// 			sites = [sites arrayByAddingObject:[[PSSite alloc] initWithProperties:siteProperties]];
// 		}
// 		CPLog.info(@"Found "+[sites count]+" sites for this user");

// 		[[[[CPApplication sharedApplication] delegate] loggedInUser] setSites:sites];

// 		var urlSelectedSiteShortName = [self shortSiteNameForCurrenHostname];
// 		if (urlSelectedSiteShortName && urlSelectedSiteShortName != "") {
// 			for (var i=0; i<[sites count]; i++) {
// 				var siteShortName = [sites[i] shortName];
// 				CPLog.info(@"Site '"+siteShortName+"' - urlSelectedSiteShortName '"+urlSelectedSiteShortName+"'");
// 				if (siteShortName == urlSelectedSiteShortName) {
// 					[self openSite:sites[i]];
// 					return;
// 				}
// 			}
// 			var shake = [[EKShakeAnimation alloc] initWithView:panel];
// 			[panel showSiteNotAvailableError];
// 		} else {
// 			if ([sites count] > 1)
// 				[panel gotoSitesModeWithSites:sites];
// 			else if ([sites count] == 1)
// 				[self openSite:sites[0]];
// 			else {
// 				var shake = [[EKShakeAnimation alloc] initWithView:panel];
// 				[panel showNoSitesAvailableError];
// 			}
// 		}
// 	}
// }

// - (void)api:(PSAPI)api didReceiveError:(id)error {
// 	if (api == loginApi) {
// 		CPLog.error("[PSLoginViewController] loginApi:didReceiveError:"+error);
// 	} else if (api == sitesApi) {
// 		CPLog.error("[PSLoginViewController] sitesApi:didReceiveError:"+error);
// 	}
// }

// - (void)showLoginPanel {
// 	[panel gotoLoginMode];
// }

// - (CPString)shortSiteNameForCurrenHostname {
// 	var hostname = window.location.hostname;
// 	var sitename = "";
// 	if (hostname && hostname != "") {
// 		var hostParts = [hostname componentsSeparatedByString:"."];
// 		sitename = hostParts[0];
// 	}
// 	if (sitename == "sam" || sitename == "sam-dev" || sitename == "sam-beta" || sitename == "www" || sitename == "10" || sitename == "127"|| sitename == "localhost") // 10 for testing from server 10.2.77....
// 		sitename = "";
// 	else
// 		CPLog.debug("hostname "+window.location.hostname+" -> sitename: "+sitename);
// 	CPLog.debug("shortSiteNameForCurrenHostname: "+sitename);

// 	return sitename;
// }

// - (void)openSite:(PSSite)site {

// 	currentMode = PSLoginViewControllerSiteLoadingMode;
// 	[panel gotoSiteLoadingMode];

// 	var userInfo = [CPDictionary dictionaryWithObject:site forKey:@"site"];
// 	var setSelectedSiteTimer = [CPTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(setSelectedSiteAfterDelay:) userInfo:userInfo repeats:NO];
// 	sitesRootNode = nil;
// }
- (void)setSelectedSiteAfterDelay:(CPTimer)aTimer {
	// var site = [[aTimer userInfo] objectForKey:@"site"];
	if (navigationController && [navigationController respondsToSelector:@selector(loginViewController:selectedSite:)])
		[navigationController loginViewController:self selectedSite:nil];
}


// -(void)addAuthenticationCookie:(CPString)aUsername password:(CPString)aPassword {
// 	// var authenticationCookie = [[CPCookie alloc] initWithName:kCookieNameAuthenticationCookie];
// 	// var date = [CPDate dateWithTimeIntervalSinceNow:kAuthenticationCookieLifetime];
// 	var authString = @""+kAuthenticationCookieUsernamePrefix+aUsername+kAuthenticationCookiePasswordPrefix+aPassword;
// 	CPLog.debug("addAuthenticationCookie - "+authString);
// 	// [authenticationCookie setValue:authString expires:date];
// 	[CPCookie cookieWithName:kCookieNameAuthenticationCookie andValue:authString forDays:kAuthenticationCookieLifetime];
// }

// -(void)removeAuthenticationCookie {
// 	CPLog.debug(@"removeAuthenticationCookie");
// 	[CPCookie removeCookieWithName:kCookieNameAuthenticationCookie];
// }

// -(CPDictionary)getAuthenticationDetailsFromCookie {
// 	CPLog.debug(@"getAuthenticationDetailsFromCookie");
// 	var authenticationCookie = [CPCookie cookieWithName:kCookieNameAuthenticationCookie];
// 	var authenticationString = [authenticationCookie value];
// 	var authParts = [authenticationString componentsSeparatedByString:kAuthenticationCookiePasswordPrefix];
// 	if ([authParts count] == 2) {
// 		var usernamePart = authParts[0];
// 		var usernamePartPrefix = [usernamePart substringToIndex:2];
// 		if (usernamePartPrefix == kAuthenticationCookieUsernamePrefix) {
// 			var cUsername = [usernamePart substringFromIndex:2];
// 			var cPassword = authParts[1];
// 			var credentials = [[CPDictionary alloc] initWithObjectsAndKeys:cUsername, PSAuthenticationCredentialsUsernameKey, cPassword, PSAuthenticationCredentialsPasswordKey];
// 			CPLog.debug(@"getAuthenticationDetailsFromCookie: credentials: "+[credentials description]);
// 			return credentials;
// 		}
// 	}
// 	CPLog.debug(@"getAuthenticationDetailsFromCookie: credentials not found");
// 	return nil;
// }

@end
