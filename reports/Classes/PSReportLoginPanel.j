/*
 * PSLoginPanel.j
 * SAM
 *
 * Created by Arjen Schipmolder on February 27, 2012.
 * Copyright 2012, Packshot Ltd All rights reserved.
 */

@import "/Projects/Packshot/SAM-web/App/PSImports.j"

@implementation PSReportLoginPanel : PSPanel {
	CPView loginView;

	// CPCheckBox rememberMeCheckbox;

	var loginButton;
	var usernameLabel;
	var usernameField;
	var passwordLabel;
	var passwordField;

	var sitesView;
	var sitesOutlineView;
	var sitesRootNode;
	var siteSelectButton;

	var siteLoadingView;
}

- (id)initWithFrame:(CGRect)aFrame {
	if (self = [super initWithFrame:aFrame]) {
		[self setShowTitleBar:NO];

		usernameLabel = [[CPTextField alloc] initWithFrame:CGRectMake(0, 1+kUnEditableTextFieldPaddingTop, 100, 29)];
		[usernameLabel setFont:[CPFont fontWithName:"BodoniURW-LigObl" size:"16"]]; // todo - Move font to theme
		[usernameLabel setTextColor:[CPColor colorWithHexString:"000000"]];
		[usernameLabel setStringValue:"Username:"];
		[usernameLabel setBezeled:NO];
		[usernameLabel setEditable:NO];

		usernameField = [[CPTextField alloc] initWithFrame:CGRectMake(80, 0, aFrame.size.width-100, 29)];
		[usernameField setAutoresizingMask:CPViewWidthSizable];
		[usernameField setPlaceholderString:"Username"];
		[usernameField setBezelStyle:CPTextFieldRoundedBezel];
		[usernameField setBezeled:YES];
		[usernameField setEditable:YES];
		[usernameField setAction:@selector(enterOnTextfield:)];

		passwordLabel = [[CPTextField alloc] initWithFrame:CGRectMake(0, 33+kUnEditableTextFieldPaddingTop, 100, 29)];
		[passwordLabel setFont:[CPFont fontWithName:"BodoniURW-LigObl" size:"16"]]; // todo - Move font to theme
		[passwordLabel setTextColor:[CPColor colorWithHexString:"000000"]];
		[passwordLabel setStringValue:"Password:"];
		[passwordLabel setBezeled:NO];
		[passwordLabel setEditable:NO];

		passwordField = [[CPSecureTextField alloc] initWithFrame:CGRectMake(80, 30, aFrame.size.width-100, 29)];
		[passwordField setAutoresizingMask:CPViewWidthSizable];
		[passwordField setPlaceholderString:""];
		[passwordField setBezelStyle:CPTextFieldRoundedBezel];
		[passwordField setBezeled:YES];
		[passwordField setEditable:YES];
		[passwordField setAction:@selector(loginButtonClicked:)];

		// rememberMeCheckbox = [CPCheckBox checkBoxWithTitle:"Remember Me"];
		// [rememberMeCheckbox setFont:[CPFont fontWithName:"BodoniURW-LigObl" size:"15"]]; // todo - Move font to theme
		// [rememberMeCheckbox setTextColor:[CPColor colorWithHexString:"000000"]];
		// [rememberMeCheckbox setFrame:CGRectMake(10, 74, 200, 17)];
		// [rememberMeCheckbox setAutoresizingMask:CPViewMaxXMargin | CPViewMaxYMargin];

		loginButton = [[PSButton alloc] initWithFrame:CGRectMake(aFrame.size.width-104, 70, 80, 24)];
		[loginButton setAutoresizingMask:CPViewMaxXMargin | CPViewMaxYMargin];
		[loginButton setTitle:"Login"];
		[loginButton setAction:@selector(loginButtonClicked:)];
		[loginButton setTarget:self];

		loginView = [[CPView alloc] initWithFrame:CGRectMake(10, 10, aFrame.size.width-20, aFrame.size.height-20)];
		[loginView setAutoresizingMask:CPViewWidthSizable | CPViewHeightSizable];
		[loginView addSubview:usernameLabel];
		[loginView addSubview:usernameField];
		[loginView addSubview:passwordLabel];
		[loginView addSubview:passwordField];
		// [loginView addSubview:rememberMeCheckbox];
		[loginView addSubview:loginButton];
		[self addSubview:loginView];

		// [usernameField becomeFirstResponder];	// makes IE f* up
		[usernameField setNextKeyView:passwordField];
		[passwordField setNextKeyView:usernameField];

		// var host = [@"" stringByAppendingString:window.location];
		// if ([[host substringToIndex:6] isEqualToString:@"file:/"]) {
			[usernameField setStringValue:"aschipmolder"];
			[passwordField setStringValue:"discovery"];
		// }

		// [usernameField setStringValue:@"admin"];
		// [passwordField setStringValue:@"discovery"];

	}
	return self;
}

- (void)rightMouseDown:(CPEvent)anEvent {
	CPLog("rightMouseDown");
	var contextualMenu = [[CPMenu alloc] initWithTitle:@"context menu"];
	var myItem = [[CPMenuItem alloc] initWithTitle:@"lorem" action:@selector(doSomething:) keyEquivalent:@"a"];
	[myItem setTarget:self];
	[myItem setEnabled:YES];
	[contextualMenu insertItem:myItem atIndex:[contextualMenu numberOfItems]];
	[CPMenu popUpContextMenu:contextualMenu withEvent:anEvent forView:self];
}

- (void)doSomething:(id)sender {
	var appDelegate = [[CPApplication sharedApplication] delegate];

	var alert = [[CPAlert alloc] init];
	[alert setDelegate:self];
	[alert setTitle:"Do something"];
	[alert setAlertStyle:CPInformationalAlertStyle];
	[alert addButtonWithTitle:@"Close"];
	[alert setMessageText:@""];
	[alert beginSheetModalForWindow:[appDelegate appWindow] modalDelegate:self didEndSelector:nil contextInfo:nil];
}

- (void)enterOnTextfield:(id)sender {
	[[self window] selectNextKeyView:[self nextKeyView]];
}

- (void)setCredentials:(CPDictionary)theCredentials {
	CPLog.debug(@"setCredentials: "+[theCredentials description]);
	CPLog.debug(@"PSAuthenticationCredentialsUsernameKey: "+PSAuthenticationCredentialsUsernameKey);
	CPLog.debug(@"PSAuthenticationCredentialsPasswordKey: "+PSAuthenticationCredentialsPasswordKey);
	var authUsername = [theCredentials objectForKey:PSAuthenticationCredentialsUsernameKey];
	if (!authUsername)
		authUsername = @"";
	var authPassword = [theCredentials objectForKey:PSAuthenticationCredentialsPasswordKey];
	if (!authPassword)
		authPassword = @"";
	[usernameField setStringValue:authUsername];
	[passwordField setStringValue:authPassword];
}

- (void)loginButtonClicked:(id)sender {
	var rememberMe = NO;
	// if ([rememberMeCheckbox state] == CPOnState)
	// 	rememberMe = YES;
	[_viewController loginWithUsername:[usernameField stringValue] password:[passwordField stringValue] rememberMe:rememberMe];
}

- (void)showLoginFailedError {
	[passwordField setStringValue:""];
	[passwordField becomeFirstResponder];
}

- (void)showNoSitesAvailableError {
	[passwordField setStringValue:""];
	[passwordField becomeFirstResponder];
}

- (void)showSiteNotAvailableError {
	[passwordField setStringValue:""];
	[passwordField becomeFirstResponder];
}

- (void)siteSelectButtonClicked:(id)sender {
	var indexes = [sitesOutlineView selectedRowIndexes];
	if ([indexes count] > 0) {
		var selectedRowIndex = [indexes firstIndex];
		var selectedSite = [[[sitesRootNode childNodes] objectAtIndex:selectedRowIndex] representedObject];
		[_viewController openSite:selectedSite];
	}
}

- (void)getRootNodeFromSites:(CPArray)sites {
	var sitesRootNode = [CPTreeNode treeNodeWithRepresentedObject:[[CPDictionary alloc] initWithObjectsAndKeys:@"Root", @"name"]];
	for (var i=0; i<[sites count]; i++) {
		[sitesRootNode insertObject:[CPTreeNode treeNodeWithRepresentedObject:[sites objectAtIndex:i]] inChildNodesAtIndex:i];
	}
	return sitesRootNode;
}

- (void)gotoLoginMode {
	sitesRootNode = [CPTreeNode treeNodeWithRepresentedObject:[[CPDictionary alloc] initWithObjectsAndKeys:@"Root", @"name"]];
	[sitesOutlineView reloadData];
	[sitesView removeFromSuperview];
	[siteLoadingView removeFromSuperview];

	var animation = [[PSResizeAnimation alloc] initWithTarget:self];
	[animation setNewSize:CGSizeMake(300, 115)];
	[animation setDuration:0];
	[animation start];

	var animation = [[PSFadeInAnimation alloc] initWithTarget:loginView];
	[animation setDuration:0];
	[animation start];

}

- (void)gotoSitesModeWithSites:(CPArray)sites {
	sitesRootNode = [self getRootNodeFromSites:sites];
	[siteLoadingView removeFromSuperview];

	var loginViewFrame = [loginView frame];
	var myFrame = [self frame];

	var siteCount = [sites count];
	var newHeight = (siteCount*(kGenericListItemHeight+4))+70;// 4 being the spacing
	if (siteCount > 3)
		newHeight = (siteCount*(kGenericListItemHeight+4))+70;

	var sitesViewFrame = CGRectMake(loginViewFrame.origin.x, loginViewFrame.origin.y, loginViewFrame.size.width+loginViewFrame.origin.x, loginViewFrame.size.height);
	var sitesOutlineScrollViewFrame = CGRectMake(0, 0, sitesViewFrame.size.width, newHeight-80);
	var sitesOutlineViewFrame = CGRectMake(0, 0, sitesOutlineScrollViewFrame.size.width-20, sitesOutlineScrollViewFrame.size.height);

	sitesView = [[CPView alloc] initWithFrame:sitesViewFrame];
    [sitesView setBackgroundColor:[CPColor clearColor]];
	[sitesView setAutoresizingMask:CPViewWidthSizable | CPViewHeightSizable];
	[sitesView setAlphaValue:0];
	[self addSubview:sitesView];

	var textColumn = [[CPTableColumn alloc] initWithIdentifier:@"TextColumn"];
	[textColumn setWidth:sitesOutlineScrollViewFrame.size.width-10];
	[textColumn setDataView:[[PSSitesListItemView alloc] initWithFrame:CGRectMakeZero()]];

	sitesOutlineView = [[PSOutlineView alloc] initWithFrame:sitesOutlineViewFrame];
	[sitesOutlineView setIntercellSpacing:CGSizeMake(0,4)];
	[sitesOutlineView setAutoresizingMask:CPViewWidthSizable | CPViewHeightSizable];
	[sitesOutlineView setIndentationPerLevel:0];
	// [sitesOutlineView setIntercellSpacing:CGSizeMake(0,0)];
	[sitesOutlineView setRowHeight:kGenericListItemHeight];
    [sitesOutlineView setBackgroundColor:[CPColor clearColor]];
	[sitesOutlineView setHeaderView:nil];
	[sitesOutlineView setCornerView:nil];
	[sitesOutlineView addTableColumn:textColumn];
	[sitesOutlineView setOutlineTableColumn:textColumn];
	[sitesOutlineView setDelegate:self];
	[sitesOutlineView setDataSource:self];
	[sitesOutlineView setAllowsMultipleSelection:NO];
	[sitesOutlineView setSelectionHighlightStyle:CPTableViewSelectionHighlightStyleNone];

	sitesOutlineScrollView = [[PSScrollView alloc] initWithFrame:sitesOutlineScrollViewFrame];
	[sitesOutlineScrollView setBorderType:CPNoBorder];
	[sitesOutlineScrollView setAutoresizingMask:CPViewWidthSizable | CPViewHeightSizable];
    [sitesOutlineScrollView setBackgroundColor:[CPColor clearColor]];
    [sitesOutlineScrollView setAutohidesScrollers:NO];
	[sitesOutlineScrollView setScrollerStyle:CPScrollerStyleOverlay];
	[sitesOutlineScrollView setHasHorizontalScroller:NO];
	[sitesView addSubview:sitesOutlineScrollView];
	[sitesOutlineScrollView setDocumentView:sitesOutlineView];

	siteSelectButton = [[PSButton alloc] initWithFrame:CGRectMake(myFrame.size.width-104, newHeight-55, 80, 24)];
	[siteSelectButton setAutoresizingMask:CPViewMaxXMargin | CPViewMaxYMargin];
	[siteSelectButton setTitle:"Select"];
	[siteSelectButton setAction:@selector(siteSelectButtonClicked:)];
	[siteSelectButton setTarget:self];
	[siteSelectButton setEnabled:NO];
	[sitesView addSubview:siteSelectButton];

	var animation = [[PSResizeAnimation alloc] initWithTarget:self];
	[animation setNewSize:CGSizeMake(300, newHeight)];
	[animation start];

	var animation = [[PSFadeOutAnimation alloc] initWithTarget:loginView];
	[animation start];

	var animation = [[PSFadeInAnimation alloc] initWithTarget:sitesView];
	[animation start];
}

- (void)gotoSiteLoadingMode {
	if (sitesView != nil) {
		var sitesViewFrame = [sitesView frame];
	} else {
		var sitesViewFrame = [loginView frame];
	}
	siteLoadingView = [[CPView alloc] initWithFrame:sitesViewFrame];
	[siteLoadingView setAutoresizingMask:CPViewWidthSizable | CPViewHeightSizable];
	[siteLoadingView setAlphaValue:0];
	[self addSubview:siteLoadingView];

	var loadingTextField = [[CPTextField alloc] initWithFrame:CGRectMake(10, 20, sitesViewFrame.size.width-20, 80)];
	[loadingTextField setAutoresizingMask:CPViewWidthSizable];
	[loadingTextField setStringValue:@"Loading...\n\nOne moment please"];
	[loadingTextField setFont:[CPFont fontWithName:kPanelTitleFont size:kPanelTitleFontSize]];
	[loadingTextField setTextColor:[CPColor colorWithHexString:@"000000"]];
	[loadingTextField setAlignment:CPCenterTextAlignment];
	[siteLoadingView addSubview:loadingTextField];

	// if (sitesView != nil) {
	// 	sitesRootNode = [CPTreeNode treeNodeWithRepresentedObject:[[CPDictionary alloc] initWithObjectsAndKeys:@"Root", @"name"]];
	// 	[sitesOutlineView reloadData];
	// 	[sitesView removeFromSuperview];
	// } else {
	// 	[loginView removeFromSuperview];
	// }

	var animation = [[PSResizeAnimation alloc] initWithTarget:self];
	[animation setNewSize:CGSizeMake(300, 120)];
	[animation start];

	var fadeOutTarget = nil;
	if (sitesView != nil)
		fadeOutTarget = sitesView;
	else
		fadeOutTarget = loginView;
	var animation = [[PSFadeOutAnimation alloc] initWithTarget:fadeOutTarget];
	[animation start];

	var animation = [[PSFadeInAnimation alloc] initWithTarget:siteLoadingView];
	[animation start];


}

- (void)outlineViewItemWasDoubleClicked:(PSOutlineView)theOutlineView {
	[self siteSelectButtonClicked:nil];
}

- (int)outlineView:(CPOutlineView)theOutlineView numberOfChildrenOfItem:(id)theItem {
    if (theItem == nil)
        theItem = sitesRootNode;
    return [[theItem childNodes] count];
}

- (id)outlineView:(CPOutlineView)theOutlineView child:(int)theIndex ofItem:(id)theItem {
    if (theItem == nil)
        theItem = sitesRootNode;
	return [[theItem childNodes] objectAtIndex:theIndex];
}

- (BOOL)outlineView:(CPOutlineView)theOutlineView isItemExpandable:(id)theItem {
	return NO;
}

- (id)outlineView:(CPOutlineView)anOutlineView objectValueForTableColumn:(CPTableColumn)theColumn byItem:(id)theItem {
	// return [[theItem representedObject] valueForKey:"name"];
	return [[theItem representedObject] title];
}

- (void)outlineViewSelectionIsChanging:(CPNotification)aNotification {
}

- (void)outlineViewSelectionDidChange:(CPNotification)aNotification {

	var indexes = [sitesOutlineView selectedRowIndexes];
	if ([indexes count] > 0) {
		var selectedRowIndex = [indexes firstIndex];
		//CPLog.debug("Selected index "+selectedRowIndex);
		// var selectedRowIndex = indexes[0];
		var selectedSiteNode = [[sitesRootNode childNodes] objectAtIndex:selectedRowIndex];
		// CPLog.debug("Selected "+[[selectedSiteNode representedObject] valueForKey:"name"]+" ("+selectedRowIndex+")");
		[siteSelectButton setEnabled:YES];
	} else {
		[siteSelectButton setEnabled:NO];
	}
}

