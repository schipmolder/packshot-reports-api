
@import "/Projects/Packshot/SAM-web/App/PSImports.j"
@import "PSReportLoginViewController.j"
@import "PSSLAReportViewController.j"

@implementation PSReportNavigationController : CPViewController {

    PSReportLoginViewController loginViewController @accessors;
    PSSLAReportViewController reportViewController @accessors;
    PSFooterBar footerBar @accessors;
    PSMenuController menuController @accessors;

    PSBarcodeScannerPanel barcodeScannerPanel;
}

- (void)loadView {
    [super loadView];

    menuController = [[PSMenuController alloc] init];

    loginViewController = [[PSReportLoginViewController alloc] init];
    [loginViewController setNavigationController:self];
    [[loginViewController view] setFrame:CGRectMake(0, 0, [[self view] frame].size.width, [[self view] frame].size.height)];
    [[loginViewController view] setCenter:[_view center]];
    [[loginViewController view] setAutoresizingMask:CPViewWidthSizable | CPViewHeightSizable];
    [[self view] addSubview:[loginViewController view]];

    [self hideMenu];
}

- (void)showMenu {
    // [menuController showMenu];
    [footerBar setFrame:CGRectMake(0, [[self view] frame].size.height-kFooterBarHeight, [[self view] frame].size.width, kFooterBarHeight)];
}

- (void)hideMenu {
    [menuController hideMenu];
    [footerBar setFrame:CGRectMake(0, [[self view] frame].size.height-kFooterBarHeight, [[self view] frame].size.width, kFooterBarHeight)];
}

- (void)loginViewController:(PSLoginViewController)aLoginViewController selectedSite:(PSSite)site {
    // [[[CPApplication sharedApplication] delegate] setLoggedInSite:site];
    // [[CPNotificationCenter defaultCenter] postNotificationName:PSSiteChangedNotification object:self userInfo:nil];
    var myFrame = [[self view] frame];
    [footerBar loadBrandedLogoForSite:site];
    [footerBar loadClientLogoForSite:site];
    // if ([site type] == PSSiteTypeAdHoc)
    //     siteViewController = [[PSAdHocSiteViewController alloc] init];
    // else if ([site type] == PSSiteTypeRetainer)
        reportViewController = [[PSSLAReportViewController alloc] init];
    // else
    //     return;

    [self showMenu];
    [reportViewController setNavigationController:self];
    [[reportViewController view] setFrame:CGRectMake(0, 0, myFrame.size.width, myFrame.size.height)];
    [[reportViewController view] setAutoresizingMask:CPViewWidthSizable | CPViewHeightSizable];
    [[self view] addSubview:[reportViewController view]];
    [reportViewController fadeInWithDuration:0.3 delegate:self];
    [loginViewController fadeOutWithDuration:0.3 delegate:self];



    [[CPNotificationCenter defaultCenter] addObserver:self selector:@selector(barcodeScanDetected:) name:PSBarcodeScanDetectedNotification object:nil];
}

- (void)barcodeScanDetected:(CPNotification)aNotification {
    if (aNotification) {
        var userInfo = [aNotification userInfo];
        var barcode = [userInfo objectForKey:@"barcode"];
        CPLog.info(@"Barcode scan '"+barcode+"'");
    }
    var barcodeScannerPanelIsOpen = [[PSPopupManager sharedManager] popupIsOfClass:@"PSBarcodeScannerPanel"];
    if (!barcodeScannerPanelIsOpen)
        [self openBarcodeScannerPanel];
        CPLog(@"barcodeScannerPanel: "+barcodeScannerPanel);
    if (barcode)
        [barcodeScannerPanel addScan:barcode];
}


- (void)openDashboard {
    [reportViewController openDashboard];
}

- (void)openDownloadManager {
    var downloadManager = [[[CPApplication sharedApplication] delegate] downloadManager];
    [downloadManager openPanel];
}


- (void) openAddProductPanel {
    var addProductPanel = [[PSAddProductPanel alloc] initWithFrame:CGRectMake(0,0,400, 290)];
    [addProductPanel setAutoresizingMask:CPViewMinXMargin | CPViewMaxXMargin | CPViewMinYMargin | CPViewMaxYMargin];
    [[PSPopupManager sharedManager] showPopup:addProductPanel];
}

- (void) openUploadPanel {
    var uploadPopupFrame = CGRectMake(0,0,240, 260);
    var uploadPopup = [[PSPanel alloc] initWithFrame:uploadPopupFrame panelStyle:PSPanelStylePopup];
    [uploadPopup setAutoresizingMask:CPViewWidthSizable | CPViewHeightSizable];
    [uploadPopup setShowTitleBar:YES];
    [uploadPopup setTitle:"UPLOAD"];

    var instructions = [[CPTextField alloc] initWithFrame:CGRectMake(20, 46, uploadPopupFrame.size.width-40, 40)];
    [instructions setTextColor:[CPColor whiteColor]];
    [instructions setAlignment:CPCenterTextAlignment];
    [uploadPopup addSubview:instructions];

    var dropView = [[CPImageView alloc] initWithFrame:CGRectMake(20, 60, 200, 200)];
    [dropView setAutoresizingMask:CPViewMinXMargin | CPViewMinYMargin];
    [dropView setImage:[[CPImage alloc] initWithContentsOfFile:[[CPBundle mainBundle] pathForResource:@"upload.png"] size:CPSizeMake(200, 200)]];
    [uploadPopup addSubview:dropView];

    var currentSite = [[[CPApplication sharedApplication] delegate] loggedInSite];
    var selectedTreeItem = [[[CPApplication sharedApplication] delegate] selectedTreeItem];
    var uploadUrl = kAPIServerUrl+"/item/upload/"+[currentSite shortName]+[selectedTreeItem path]+"/";
    CPLog.debug("uploadUrl: "+uploadUrl);
    // activate the view as a drop zone
    var fileDropUploadController = [[DCFileDropController alloc] initWithView:dropView
                                                                 dropDelegate:self
                                                                    uploadURL:[CPURL URLWithString:uploadUrl]
                                                                uploadManager:[DCFileUploadManager sharedManager]];

    [[PSPopupManager sharedManager] showPopup:uploadPopup];

    // create a window to show the upload progress
    var menuFrame = [[self view] frame];
    var uploadsPanel = [[DCFileUploadsPanel alloc] initWithFrame:CGRectMake(menuFrame.size.width-260, 80.0, 250.0, 300.0)];
    [[DCFileUploadManager sharedManager] setDelegate:uploadsPanel];
    [uploadsPanel orderFront:nil];

    if (CPFeatureIsCompatible(CPHTMLDragAndDropFeature)) {
        CPLog.debug("CPFeatureIsCompatible(CPHTMLDragAndDropFeature): true");
        // drop upload
        [instructions setStringValue:"To upload your files drop them\ninto the box below."];
    } else {
        CPLog.debug("CPFeatureIsCompatible(CPHTMLDragAndDropFeature): false");
        // non-drop upload
        [instructions setStringValue:"Click the box below to select\nthe files you want to upload."];
    }
}

- (void) openImportPanel {

/*
    var uploadPopupFrame = CGRectMake(0,0,240, 260);
    var uploadPopup = [[PSPanel alloc] initWithFrame:uploadPopupFrame panelStyle:PSPanelStylePopup];
    [uploadPopup setAutoresizingMask:CPViewWidthSizable | CPViewHeightSizable];
    [uploadPopup setShowTitleBar:YES];
    [uploadPopup setTitle:"UPLOAD EXTRACT FILE"];

    var instructions = [[CPTextField alloc] initWithFrame:CGRectMake(20, 46, uploadPopupFrame.size.width-40, 40)];
    [instructions setTextColor:[CPColor whiteColor]];
    [instructions setAlignment:CPCenterTextAlignment];
    [uploadPopup addSubview:instructions];

    var dropView = [[CPImageView alloc] initWithFrame:CGRectMake(20, 60, 200, 200)];
    [dropView setAutoresizingMask:CPViewMinXMargin | CPViewMinYMargin];
    [dropView setImage:[[CPImage alloc] initWithContentsOfFile:[[CPBundle mainBundle] pathForResource:@"upload.png"] size:CPSizeMake(200, 200)]];
    [uploadPopup addSubview:dropView];

    var currentSite = [[[CPApplication sharedApplication] delegate] loggedInSite];
    var selectedTreeItem = [[[CPApplication sharedApplication] delegate] selectedTreeItem];
    var uploadUrl = kAPIServerUrl+"/extract/import/"+[currentSite shortName]+"/";
    CPLog.debug("uploadUrl: "+uploadUrl);
    // activate the view as a drop zone
    var fileDropUploadController = [[DCFileDropController alloc] initWithView:dropView
                                                                 dropDelegate:self
                                                                    uploadURL:[CPURL URLWithString:uploadUrl]
                                                                uploadManager:[DCFileUploadManager sharedManager]];

    [[PSPopupManager sharedManager] showPopup:uploadPopup];

*/
    var appDelegate = [[CPApplication sharedApplication] delegate];
    var appWindow = [appDelegate appWindow];
    var importPanel = [[PSExtractImportPanel alloc] init];
    [importPanel beginSheetModalForWindow:appWindow modalDelegate:self didEndSelector:nil contextInfo:nil];

    // // create a window to show the upload progress
    // var menuFrame = [[self view] frame];
    // // var uploadsPanel = [[DCFileUploadsPanel alloc] initWithFrame:CGRectMake(menuFrame.size.width-260, 80.0, 250.0, 300.0)];
    // [[DCFileUploadManager sharedManager] setDelegate:self];
    // // [uploadsPanel orderFront:nil];

    // if (CPFeatureIsCompatible(CPHTMLDragAndDropFeature)) {
    //  CPLog.debug("CPFeatureIsCompatible(CPHTMLDragAndDropFeature): true");
    //  // drop upload
    //  [instructions setStringValue:"To upload your files drop them\ninto the box below."];
    // } else {
    //  CPLog.debug("CPFeatureIsCompatible(CPHTMLDragAndDropFeature): false");
    //  // non-drop upload
    //  [instructions setStringValue:"Click the box below to select\nthe files you want to upload."];
    // }
}

// - (void)fileUploadManagerDidChange:(DCFileUploadManager)theManager {
//  CPLog.debug("fileUploadManagerDidChange");
//  // [tableView reloadData];
//  // [[CPRunLoop currentRunLoop] limitDateForMode:CPDefaultRunLoopMode];
// }


- (void) openDownloadPanel {
    var downloadPopup = [[PSDownloadPanel alloc] initWithFrame:CGRectMake(0,0,350, 450)];
    [downloadPopup setAutoresizingMask:CPViewWidthSizable | CPViewHeightSizable];
    [[PSPopupManager sharedManager] showPopup:downloadPopup];
}

- (void) openBarcodeScannerPanel {
    var appDelegate = [[CPApplication sharedApplication] delegate];
    var appWindow = [appDelegate appWindow];
    var appContentView = [appWindow contentView];

    var panelFrame = CGRectMake(0,0,[appContentView frame].size.width*.8, [appContentView frame].size.height*.8);
    barcodeScannerPanel = [[PSBarcodeScannerPanel alloc] initWithFrame:panelFrame];
    [barcodeScannerPanel setAutoresizingMask:CPViewWidthSizable | CPViewHeightSizable];
    [[PSPopupManager sharedManager] showPopup:barcodeScannerPanel];
}


- (void) openAdminPanel {

    var appDelegate = [[CPApplication sharedApplication] delegate];
    var appWindow = [appDelegate appWindow];
    var appContentView = [appWindow contentView];

    var adminPanelFrame = CGRectMake(0,0,[appContentView frame].size.width*.8, [appContentView frame].size.height*.8);
    // var adminPanel = [[PSAdminPanel alloc] initWithFrame:adminPanelFrame];
    var adminPanel = [[PSAdminPanel alloc] initWithContentRect:adminPanelFrame styleMask:CPDocModalWindowMask];

    [adminPanel setAutoresizingMask:CPViewWidthSizable | CPViewHeightSizable];

    // var backgroundImageView = [[CPImageView alloc] initWithFrame:CGRectMake(0,0,adminPanelFrame.size.width, adminPanelFrame.size.height)];
    // [backgroundImageView setAutoresizingMask:CPViewWidthSizable | CPViewHeightSizable];
    // [backgroundImageView setImage:[[CPImage alloc] initWithContentsOfFile:"Resources/Images/background_rounded.png"]];
    // [backgroundImageView addSubview:adminPanel];

    // var adminPanel = [[CPAlert alloc] init];
    // [adminPanel setWindowStyle:CPInformationalAlertStyle];
    // // [alert setTitle:"Download"];
    // // [alert setMessageText:"You have to select at least 1 item to start a download"];
    // // [alert setInformativeText:"Please select the item you want to download from the list below and open the Download menu again."];
    // [adminPanel beginSheetModalForWindow:[appDelegate appWindow] modalDelegate:self didEndSelector:nil contextInfo:nil];

    [CPApp beginSheet:adminPanel modalForWindow:appWindow modalDelegate:self didEndSelector:nil contextInfo:nil];
}

// - (void) logout {
//     // called like this [[[[CPApplication sharedApplication] delegate] navigationController] logout];
//     // called from e.g. AppDelegate (on 401 response) and from menu button

//     [[CPNotificationCenter defaultCenter] removeObserver:self name:PSBarcodeScanDetectedNotification object:nil];

//     [footerBar unloadClientLogo];
//     [self hideMenu];
//     [loginViewController showLoginPanel];

//     [reportViewController willBeClosed];

//     [loginViewController fadeInWithDuration:0.3 delegate:self];
//     [siteViewController fadeOutWithDuration:0.3 delegate:self];

//     [[[CPApplication sharedApplication] delegate] setLoggedInUser:nil];
//     [[[CPApplication sharedApplication] delegate] setLoggedInSite:nil];
//     [[CPNotificationCenter defaultCenter] postNotificationName:PSUserLoggedOutNotification object:self userInfo:nil];
// }

// ******************** DCFileDropControllerDropDelegate *********************
- (void)fileDropUploadController:(DCFileDropController)theController setState:(BOOL)visible {
}

// ******************** Viewcontroller animation delegates *********************

- (BOOL)viewController:(PSViewController)aViewController fadeInAnimationShouldStart:(id)animation {
    return YES;
}

- (void)viewController:(PSViewController)aViewController fadeInAnimationDidEnd:(id)animation {
}

- (void)viewController:(PSViewController)aViewController fadeInAnimationDidStop:(id)animation {
}


- (BOOL)viewController:(PSViewController)aViewController fadeOutAnimationShouldStart:(id)animation {
    return YES;
}

// - (void)viewController:(PSViewController)aViewController fadeOutAnimationDidEnd:(id)animation {
//     if (aViewController == reportViewController) {
//         [reportViewController dealloc];
//         reportViewController = nil;
//     }
// }

- (void)viewController:(PSViewController)aViewController fadeOutAnimationDidStop:(id)animation {
}

@end
