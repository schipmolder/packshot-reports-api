/*
 * PSReportViewController.j
 * SAM
 *
 * Created by Arjen Schipmolder on February 27, 2012.
 * Copyright 2012, Packshot Ltd All rights reserved.
 */

@import "/Projects/Packshot/SAM-web/App/PSImports.j"

@import "../DatePicker.j"
@import "../Stepper.j"
@import "Model/PSSMReportObject.j"
@import "PSSMObjectCell.j"
@import "graph.j"

@implementation PSSLAReportViewController : PSViewController {
	DatePicker fromDatePicker @accessors;
	DatePicker toDatePicker @accessors;
	CPButton goButton @accessors;

	CPSegmentedControl segmentedControl @accessors;

	// CPSplitView resultsSplitView @accessors;

	CPBox box @accessors;
	CPView resultsSummaryView @accessors;
	CPScrollView resultsScrollView @accessors;
	CPTableView resultsTableView @accessors;
	CPArray data @accessors;

	var pieChart @accessors;

	CPURLRequest apiRequest @accessors;
	CPString apiResponseData @accessors;

	int unknownCounter;
	int within2Counter;

}

- (void)loadView {
	[super loadView];

	fromDatePicker = [[DatePicker alloc] initWithFrame:CGRectMake(100,100,100,30)];
    [fromDatePicker displayPreset:1];
    [fromDatePicker setDelegate:self];
    [[self view] addSubview:fromDatePicker];

	toDatePicker = [[DatePicker alloc] initWithFrame:CGRectMake(250,100,100,30)];
    [toDatePicker displayPreset:1];
    [toDatePicker setDelegate:self];
    [[self view] addSubview:toDatePicker];

    goButton = [[CPButton alloc] initWithFrame:CGRectMake(400, 100, 40, 26)]
    [goButton setTitle:@"Go"];
    [goButton setTarget:self];
    [goButton setAction:@selector(go:)];
    [[self view] addSubview:goButton];

	data = [[CPArray alloc] init];




	var aFrame = [[self view] frame];


	segmentedControl = [[CPSegmentedControl alloc] initWithFrame:CGRectMake(0, 150, 0, 30)];
	[segmentedControl setAutoresizingMask:CPViewMinXMargin | CPViewMaxXMargin];
	[segmentedControl setCenter:CGPointMake(aFrame.size.width/2, 165)];
	[segmentedControl setTarget:self];
	[segmentedControl setAction:@selector(segmentedControlChanged:)];
    [segmentedControl setSegmentCount:3];
    [segmentedControl setLabel:@"Summary" forSegment:0];
    [segmentedControl setLabel:@"Graph" forSegment:1];
    [segmentedControl setLabel:@"Raw Data" forSegment:2];


	box = [[CPBox alloc] initWithFrame:CGRectMake(10, 165, aFrame.size.width-20, aFrame.size.height-175)];
	[box setAutoresizingMask:CPViewWidthSizable | CPViewHeightSizable];
	// [box setBackgroundColor:[CPColor greenColor]];
	[box setBorderColor:[CPColor grayColor]];
	[box setCornerRadius:6];
	[box setBorderType:CPLineBorder];
	[[self view] addSubview:box];

	var boxBounds = [box bounds];

	resultsSummaryView = [self newSummaryView];
	// [resultsSummaryView setHidden:YES];
	[box addSubview:resultsSummaryView];

	pieChart = [self newGraphView];
	[pieChart setHidden:YES];
	[box addSubview:pieChart];

	resultsScrollView = [self newDataView];
	[resultsScrollView setHidden:YES];
	[box addSubview:resultsScrollView];

	[[self view] addSubview:segmentedControl];

	[segmentedControl setSelectedSegment:0];

}

- (CPView)newSummaryView {
	var boxBounds = [box bounds];
	summaryView = [[CPView alloc] initWithFrame:CGRectMake(1, 20, boxBounds.size.width-2, boxBounds.size.height-40)];
	[summaryView setBackgroundColor:[CPColor redColor]];
	[summaryView setAutoresizingMask:CPViewWidthSizable | CPViewHeightSizable];
	return summaryView;
}

- (Graph)newGraphView {
	var boxBounds = [box bounds];
	var aGraph = [[Graph alloc] initWithFrame:CGRectMake(1, 20, boxBounds.size.width-2, boxBounds.size.height-40)];
	[aGraph setDelegate:self];
	[aGraph setAutoresizingMask:CPViewWidthSizable | CPViewHeightSizable];
	return aGraph;
}

- (CPScrollView)newDataView {
	var boxBounds = [box bounds];

	scrollView = [[CPScrollView alloc] initWithFrame:CGRectMake(1, 20, boxBounds.size.width-2, boxBounds.size.height-40)];
	[scrollView setBackgroundColor:[CPColor clearColor]];
	[scrollView setScrollerStyle:CPScrollerStyleOverlay];
	[scrollView setAutohidesScrollers:NO];
	[scrollView setAutoresizingMask:CPViewWidthSizable | CPViewHeightSizable];


	resultsTableView = [[CPTableView alloc] init];
	[resultsTableView setAutoresizingMask:CPViewWidthSizable | CPViewHeightSizable];
	[resultsTableView setRowHeight:30];
	[resultsTableView setCornerView:nil];
	[resultsTableView setAllowsColumnSelection:NO];
	[resultsTableView setDelegate:self];
	[resultsTableView setDataSource:self];
	[resultsTableView setTarget:self];
	[scrollView setDocumentView:resultsTableView];


	var skuColumn = [[CPTableColumn alloc] initWithIdentifier:@"sku"];
	[[skuColumn headerView] setStringValue:@"SKU"];
	[skuColumn setWidth:200];
	// [skuColumn bind:@"value" toObject:dataSourceController withKeyPath:@"arrangedObjects.sku" options:nil];
	[resultsTableView addTableColumn:skuColumn];

	var pickedColumn = [[CPTableColumn alloc] initWithIdentifier:@"picked"];
	[[pickedColumn headerView] setStringValue:@"Picked/Ordered"];
	[pickedColumn setWidth:200];
	// [skuColumn bind:@"value" toObject:dataSourceController withKeyPath:@"arrangedObjects.sku" options:nil];
	[resultsTableView addTableColumn:pickedColumn];

	var sentColumn = [[CPTableColumn alloc] initWithIdentifier:@"sentForPhotography"];
	[[sentColumn headerView] setStringValue:@"Sent for Photography"];
	// [sentColumn bind:@"value" toObject:dataSourceController withKeyPath:@"arrangedObjects.sentForPhotography" options:nil];
	[resultsTableView addTableColumn:sentColumn];

	var photographColumn = [[CPTableColumn alloc] initWithIdentifier:@"photograph"];
	[[photographColumn headerView] setStringValue:@"Photograph"];
	// [photographColumn bind:@"value" toObject:dataSourceController withKeyPath:@"arrangedObjects.photograph" options:nil];
	[resultsTableView addTableColumn:photographColumn];

	var enrichmentColumn = [[CPTableColumn alloc] initWithIdentifier:@"enrichment"];
	[[enrichmentColumn headerView] setStringValue:@"Text Enrichment Date"];
	// [enrichmentColumn bind:@"value" toObject:dataSourceController withKeyPath:@"arrangedObjects.enrichment" options:nil];
	[resultsTableView addTableColumn:enrichmentColumn];

	var deliveryDateColumn = [[CPTableColumn alloc] initWithIdentifier:@"deliveryDate"];
	[[deliveryDateColumn headerView] setStringValue:@"Delivery Date"];
	// [deliveryDateColumn bind:@"value" toObject:dataSourceController withKeyPath:@"arrangedObjects.deliveryDate" options:nil];
	[resultsTableView addTableColumn:deliveryDateColumn];

	// [resultsTableView bind:@"content" toObject:dataSourceController withKeyPath:@"contentArray" options:nil];

	return scrollView;
}

- (void)segmentedControlChanged:(id)sender {
	switch([segmentedControl selectedSegment]) {
		case 0:
			[resultsSummaryView setHidden:NO];
			[pieChart setHidden:YES];
			[resultsScrollView setHidden:YES];
		break;
		case 1:
			[resultsSummaryView setHidden:YES];
			[pieChart setHidden:NO];
			[resultsScrollView setHidden:YES];
		break;
		case 2:
			[resultsSummaryView setHidden:YES];
			[pieChart setHidden:YES];
			[resultsScrollView setHidden:NO];
		break;
	}
}

#pragma mark CPTableview Datasource

- (int)numberOfRowsInTableView:(CPTableView)aTableView {
	return [data count];
}

- (id)tableView:(CPTableView)aTableView objectValueForTableColumn:(CPTableColumn)aColumn row:(int)aRowIndex {
	var object = [data objectAtIndex:aRowIndex];


	var columnIdentifier = [aColumn identifier];

	if ([columnIdentifier isEqualToString:@"sku"])
		return object.sku;
	else if ([columnIdentifier isEqualToString:@"picked"])
		return object.picked;
	else if ([columnIdentifier isEqualToString:@"sentForPhotography"])
		return object.sentForPhotography;
	else if ([columnIdentifier isEqualToString:@"photograph"])
		return object.photograph;
	else if ([columnIdentifier isEqualToString:@"enrichment"])
		return object.enrichment;
	else if ([columnIdentifier isEqualToString:@"deliveryDate"])
		return object.deliveryDate;
	return @"";
}

-(void)datePickerDidChange:(id)sender {
	if([sender object] === fromDatePicker){
		[toDatePicker setMinDate: [fromDatePicker date]];
		console.log("fromDatePicker: "+[[sender object] date]);
	} else {
		console.log("toDatePicker: "+[[sender object] date]);
	}
}

- (void)go:(id)sender {
	[fromDatePicker setEnabled:NO];
	[toDatePicker setEnabled:NO];
	[goButton setEnabled:NO];

/*
	apiRequest = [[CPURLRequest alloc] initWithURL:@"http://cs.packshot.com/api/bijenkorf/report2?month=02&year=2013&format=json"];
	[apiRequest setHTTPMethod:@"GET"];
	[apiRequest setValue:"text/plain;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
*/

	var path = [[CPBundle mainBundle] pathForResource:@"test.json"];
	var apiRequest = [CPURLRequest requestWithURL:path],

	apiResponseData = [[CPString alloc] initWithString:@""];
	var urlConnection = [CPURLConnection connectionWithRequest:apiRequest delegate:self];
	[urlConnection start];

	data = [[CPArray alloc] init];
	[resultsTableView reloadData];
}

-(void)connection:(CPURLConnection)connection didFailWithError:(id)error {
	CPLog.trace(@"didFailWithError: "+error+" - "+[apiRequest URL]);
}

-(void)connection:(CPURLConnection)connection didReceiveResponse:(CPHTTPURLResponse)response {
	// CPLog.trace(@"didReceiveResponse "+[apiRequest URL]);
}

-(void)connection:(CPURLConnection)connection didReceiveData:(CPString)theData {
	if (apiResponseData != null)
		apiResponseData += theData;
}

-(void)connectionDidFinishLoading:(CPURLConnection)connection {

	var httpRequest = [connection _HTTPRequest];
	var statusCode = httpRequest.status();

	var response = [apiResponseData objectFromJSON];
	apiResponseData = [[CPString alloc] initWithString:@""];

	unknownCounter = 0;
	within2Counter = 0;

	for (var i=0; i<[response.reportRecords count]; i++) {
		var objectProperties = [CPDictionary dictionaryWithJSObject:response.reportRecords[i]];//[CPDictionary dictionaryWithJSObject:[[CPDictionary dictionaryWithJSObject:response[i]] objectForKey:"property"]];
		var object = [[PSSMReportObject alloc] init];
		[object setSku:[objectProperties objectForKey:@"sku"]];
		[object setPicked:[objectProperties objectForKey:@"picked"]];
		[object setSentForPhotography:[objectProperties objectForKey:@"sentForPhotographyDate"]];
		[object setPhotograph:[objectProperties objectForKey:@"photographyDate"]];
		[object setEnrichment:[objectProperties objectForKey:@"textEnrichmentDate"]];
		[object setDeliveryDate:[objectProperties objectForKey:@"deliveryDate"]];
		data = [data arrayByAddingObject:object];

		if (object.photograph != nil && ![object.photograph isEqualToString:@""])
			within2Counter++;
		else
			unknownCounter++;

	}
	[resultsTableView reloadData];
	[self drawGraph];
}

- (void)drawGraph {
	[pieChart addPlot:{title:@"Products", data: [[0, within2Counter], [1, unknownCounter]]}];
}

- (void) graphViewDidFinishLoading:(Graph)graph {

}

- (BOOL)splitView:(CPSplitView)aSplitView shouldAdjustSizeOfSubview:(CPView)aSubView {
	return YES;
}
@end
