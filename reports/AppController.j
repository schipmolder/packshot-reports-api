/*
 * AppController.j
 * reports
 *
 * Created by You on March 4, 2013.
 * Copyright 2013, Your Company All rights reserved.
 */

@import <Foundation/CPObject.j>
@import "/Projects/Packshot/SAM-web/App/PSImports.j"
@import "Classes/PSReportNavigationController.j"

SAM_THEME = @"SAMStudio";

@implementation AppController : CPObject {
    PSWindow appWindow @accessors;
    PSReportNavigationController navigationController @accessors;
    PSUser loggedInUser @accessors;
}

- (void)applicationDidFinishLaunching:(CPNotification)aNotification {
    var bundle = [CPBundle mainBundle];
    var blendName = [SAM_THEME stringByAppendingString:@".blend"];
    CPLog.info("Loading '"+SAM_THEME+"' theme");
    var blend = [[CPThemeBlend alloc] initWithContentsOfURL:[bundle pathForResource:blendName]];
    [blend loadWithDelegate:self];
}

- (void)blendDidFinishLoading:(CPThemeBlend)theBlend {
    CPLog.info("'"+SAM_THEME+"' theme loaded");

    appWindow = [[CPWindow alloc] initWithContentRect:CGRectMakeZero() styleMask:CPBorderlessBridgeWindowMask],
        contentView = [appWindow contentView];

    // loginController = [[CPViewController alloc] init];
    // [[loginController view] setFrame:CGRectMake(0,0,400,400)];
    // [[loginController view] setBackgroundColor:[CPColor redColor]];
    // [contentView addSubview:[loginController view]];

    var contentFrame = [contentView frame];

    navigationController = [[PSReportNavigationController alloc] init];
    [[navigationController view] setFrame:CGRectMake(0, 0, contentFrame.size.width, contentFrame.size.height)];
    // [[navigationController view] setCenter:[contentView center]];
    [[navigationController view] setAutoresizingMask:CPViewWidthSizable | CPViewHeightSizable];
    [contentView addSubview:[navigationController view]];
    [appWindow orderFront:self];

    // Uncomment the following line to turn on the standard menu bar.
    //[CPMenu setMenuBarVisible:YES];
}

- (BOOL)userIsCustomer {
    return YES;
}


@end
